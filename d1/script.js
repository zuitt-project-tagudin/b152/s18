console.log("hello js objects");

const grades = [91, 92, 93, 94, 95];

const objGrades = {
	firstName: "Aaron",
	lastName: "Delos Santos",
	firstGrading: 91,
	subject: "English",
	teachers: ["Carlo", "Masahiro"],
	isActive: true,
	schools: {
		city: "Manila",
		country: "Philippines"
	},
	studentNames: [ 
		{
			name: "Adrian",
			batch: "152"
		},
		{
			name: "Nikko",
			batch: "152"
		}
	],
	description: function() {
		return `${this.subject}: ${this.firstGrading} of students ${this.studentNames[0].name} and ${this.studentNames[1].name}`
	}
}

/*
	objReference.propertyName
	objReference["propertyName"]
*/

console.log(objGrades.description());
console.log(objGrades["firstName"]);



console.log(objGrades.studentNames[1].batch);

objGrades.semester = "first";

delete objGrades.semester;

const studentGrades = [
    { studentId: 1, Q1: 89.3, Q2: 91.2, Q3: 93.3, Q4: 89.8 },
    { studentId: 2, Q1: 69.2, Q2: 71.3, Q3: 76.5, Q4: 81.9 },
    { studentId: 3, Q1: 95.7, Q2: 91.4, Q3: 90.7, Q4: 85.6 },
    { studentId: 4, Q1: 86.9, Q2: 74.5, Q3: 83.3, Q4: 86.1 },
    { studentId: 5, Q1: 70.9, Q2: 73.8, Q3: 80.2, Q4: 81.8 }
];

function getAve(obj) {
	ave = (obj.Q1 + obj.Q2 + obj.Q3 + obj.Q4) / 4;
	obj.average = parseFloat(ave.toFixed(1)) ;
	console.log(obj.studentId, obj.average);
}

function listAve(arr) {
	arr.forEach(function(a) {
		getAve(a);
	})
}


listAve(studentGrades);
console.log(typeof studentGrades[0].average);



function Dog(name, breed, age) {
	this.name = name;
	this.breed = breed;
	this.age = age * 7;
	this.bark = function() {
		console.log("woof")
	}
}

let dog1 = new Dog("Bolt", "Corgi", 5);
console.log(dog1);












