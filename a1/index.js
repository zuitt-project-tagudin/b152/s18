let trainer = {
	name: "Ash",
	age: "10",
	pokemon: ["pidgey", "geodude", "metapod", "spearow"],
	friends: [{
		town: "Pallet",
		people: ["Prof Oak", "Mom", "Gary"]
	}, {
		town: "Pewter",
		people: ["Brock", "Old Man"]
	}, {
		town: "Cerulean",
		people: ["Misty", "Bill", "Officer Jenny"]
	}
	],
	talk: function() {
		console.log(`I choose you Pikachu!`);
	}

}

trainer.talk();

console.log(trainer.age);
console.log(trainer["name"]);
console.log(trainer.pokemon[2]);
console.log(trainer.friends[1].town)
console.log(trainer.friends[0]["people"])
console.log(trainer.friends[2].people)




function Pokemon(name, lvl) {
	this.name = name,
	this.level = lvl,
	this.health = lvl * 5,
	this.attack = lvl * 2,
	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name}`);
		target.health -= this.attack;
		console.log(`${target.name}'s health is now reduced to ${target.health}`);
		target.faint();
	},
	this.faint = function() {
		if (this.health <= 10) {
			console.log(`${this.name}'s health has fallen below 10, ${this.name} fainted.`)

		}
	}
}

let pidgey = new Pokemon("Pidgey", 5);
let geodude = new Pokemon("Geodude", 7)
let rattata = new Pokemon("Rattata", 4);
let metapod = new Pokemon("Metapod", 6);
let spearow = new Pokemon("Spearow", 4);

//console.log(pidgey, geodude, rattata, metapod, spearow);

console.log(pidgey);
console.log(geodude);

pidgey.tackle(geodude);
pidgey.tackle(geodude);
pidgey.tackle(geodude);

